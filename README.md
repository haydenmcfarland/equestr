# equestr

### Summary:

A 2D trivia game and satirical dating simulator featuring horses developed in Unity with C#.

![gameplay](https://haydenmcfarland.github.io/haydenmcfarland/images/equestr.gif)

[Download](https://haydenmcfarland.github.io/haydenmcfarland/downloads/equestr_demo.zip)
